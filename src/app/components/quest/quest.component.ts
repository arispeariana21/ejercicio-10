import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions,FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-quest',
  templateUrl: './quest.component.html',
  styleUrls: ['./quest.component.css']
})
export class QuestComponent implements OnInit {

  formc!: FormGroup;

  constructor(private fb: FormBuilder) { 
    this.crearCuestionario()
  }

  ngOnInit(): void {
  }

  get aceptarNoValido(){
    return this.formc.get('aceptar')?.invalid && this.formc.get('aceptar')?.touched
  }

  crearCuestionario():void{
    this.formc = this.fb.group({
      answer1:[''],
      answer2: [''],
      answer3: [''],
      answer4: [''],
      answer5: [''],
      aceptar: ['']
    })
  }

  save(): void{   
    console.log(this.formc.value);
    console.log(this.formc.value.answer1);
    console.log(this.formc.value.answer2);
    console.log(this.formc.value.answer3);
    console.log(this.formc.value.answer4);
    console.log(this.formc.value.answer5);
    
  }
}
